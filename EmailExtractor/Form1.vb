
Public Class Form1

    Dim o As ExtractPst
    Dim allMessages As String = ""


    'Private Sub o_DocEmailComplete() Handles o.DocEmailComplete
    '    MsgBox("Email extraction done successfully....")
    'End Sub
    Private Function funPath(ByVal FullPath As String)
        Dim path As String
        Dim File As String

        path = FullPath
        Dim strArray() As String = path.Split("\")

        File = strArray(strArray.Length - 1)

        path = path.Replace(File, Nothing)

        Return path
    End Function

    Private Sub btnBrowse1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse1.Click
        ' First create a FolderBrowserDialog object
        Dim FolderBrowserDialog1 As New FolderBrowserDialog

        ' Then use the following code to create the Dialog window
        ' Change the .SelectedPath property to the default location
        With FolderBrowserDialog1
            Try
                .SelectedPath = Me.txtSrc.Text
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
            ' Select the C:\Windows directory on entry.
            .SelectedPath = "c:\windows"
            ' Prompt the user with a custom message.
            .Description = "Select the source directory"
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Me.txtSrc.Text = .SelectedPath
            End If
        End With
    End Sub

    Private Sub btnBrowse2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse2.Click
        ' First create a FolderBrowserDialog object
        Dim FolderBrowserDialog1 As New FolderBrowserDialog

        ' Then use the following code to create the Dialog window
        ' Change the .SelectedPath property to the default location
        With FolderBrowserDialog1
            Try
                .SelectedPath = Me.txtDest.Text
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
            ' Select the C:\Windows directory on entry.
            .SelectedPath = "c:\windows"
            ' Prompt the user with a custom message.
            .Description = "Select the source directory"
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Me.txtDest.Text = .SelectedPath
            End If
        End With
    End Sub

    Private Sub btnBrowse3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse3.Click
        ' First create a FolderBrowserDialog object
        Dim FolderBrowserDialog1 As New FolderBrowserDialog

        ' Then use the following code to create the Dialog window
        ' Change the .SelectedPath property to the default location
        With FolderBrowserDialog1
            Try
                .SelectedPath = Me.txtMove.Text
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try
            ' Select the C:\Windows directory on entry.
            .SelectedPath = "c:\windows"
            ' Prompt the user with a custom message.
            .Description = "Select the source directory"
            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                Me.txtMove.Text = .SelectedPath
            End If
        End With
    End Sub

  

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
  
    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            If Not IO.Directory.Exists(Application.StartupPath + "\" + allMessages) Then
                IO.Directory.CreateDirectory(Application.StartupPath + "\" + allMessages)
            End If
            o = New ExtractPst
            o.RecursiveEmail(txtSrc.Text, txtDest.Text, txtMove.Text)
            MsgBox("Email extraction process completed successfully....")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ToolTip1.SetToolTip(Me.Button1, "Runs the Program")
        ToolTip1.SetToolTip(Me.btnExit, "Exit")
    End Sub
End Class
