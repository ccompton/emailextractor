


Imports System.IO
Imports Microsoft.Office.Core
Imports Microsoft.Office.Interop
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop.Outlook



Public Class ExtractPst
    Implements IDisposable

    'Outlook Object
    Private mOulook As New Outlook.Application

    Dim objApp As Microsoft.Office.Interop.Outlook.Application
    Dim objNsp As Microsoft.Office.Interop.Outlook.NameSpace
    Shared cnt As Int16 = 0
    Shared strDest As String
    Shared MsgPath As String

    Public Sub RecursiveEmail(ByVal sourceDir As String, ByVal destDir As String, ByVal strMovePath As String)
        Dim posSep As Integer = 0
        Dim fileExt As String = ""
        Dim fileNameWithoutExt As String = ""
        Dim aDirs() As String = Nothing
        Dim sFile As String = ""
        Dim aFiles() As String = Nothing
        strDest = strMovePath
        Dim objapp As Outlook.Application

        Dim i As Integer = 0

        If Not sourceDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            sourceDir &= System.IO.Path.DirectorySeparatorChar
        End If

        If Not destDir.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then
            destDir &= System.IO.Path.DirectorySeparatorChar
        End If

        Try
            ' Get the files from the current parent.
            aFiles = System.IO.Directory.GetFiles(sourceDir, "*.pst")

            objApp = New Outlook.Application
            objNsp = objApp.GetNamespace("MAPI")
            Dim strFileName As String = ""
            For Each pst As String In aFiles
                Try
                    Dim Id As Integer = pst.LastIndexOf("\") + 1
                    strFileName = pst.Substring(Id, pst.Length - Id)
                    ExtractOutlookPst(sourceDir, strFileName, destDir)
                Catch ex As SystemException
                End Try
            Next
        Catch ex As SystemException
            MsgBox(ex.Message)
        Finally
            objNsp = Nothing
            objApp.Quit()
            mOulook.Quit()
            Dispose()
        End Try
    End Sub

    Private Sub ExtractOutlookPst(ByVal srcPath As String, ByVal fileName As String, ByVal emailMovePath As String)

        Dim objFolder As Microsoft.Office.Interop.Outlook.MAPIFolder = Nothing
        Dim objItem As Object = Nothing
        Dim strFolderName As String
        Dim MailBoxId As String = ""
        Dim MailBoxName As String = ""
        Dim MailBoxPath As String = ""
        Try
            If Not srcPath.EndsWith(System.IO.Path.DirectorySeparatorChar.ToString()) Then                ' Add trailing separators to the supplied paths if they don't exist.
                srcPath &= System.IO.Path.DirectorySeparatorChar
            End If

            If Not Directory.Exists(emailMovePath) Then
                Directory.CreateDirectory(emailMovePath)
            End If
            'Logon to this session of Pst Processing 
            objNsp.Session.Logon("outlook", "", False, True)
            'Add Store to this session so all the items can be processed in the store
            objNsp.Session.AddStore(srcPath + fileName)

            objFolder = objNsp.Session.Folders.GetLast
            MailBoxId = objFolder.StoreID
            MailBoxName = objFolder.Name
            MailBoxPath = srcPath + fileName

            If Not IsNothing(objFolder) Then
                If srcPath <> "" Or IsDBNull(srcPath) Then
                    strFolderName = objFolder.Name
                    srcPath = srcPath & strFolderName
                    If Directory.Exists(srcPath) Then
                        MsgBox("This folder has already been exported here. Please clear the destination or choose another.")
                        Exit Sub
                    Else
                        'Process all items for this folder
                        Call ProcessFolder(objFolder, emailMovePath)
                    End If
                End If
            End If

        Catch ex As SystemException
            MsgBox(ex.Message)
        Finally
            'Log off to this session of namespace
            objNsp.Session.Logoff()
            'Remove store from this session
            objNsp.Session.RemoveStore(objFolder)
            objFolder = Nothing
        End Try
    End Sub

    Private Sub ProcessFolder(ByVal StartFolder As Microsoft.Office.Interop.Outlook.MAPIFolder, ByVal strPath As String)

        'Following statement will create directory for each of outlook PST folder.. 
        Directory.CreateDirectory(strPath)
        ' process all the items in this folder
        Dim i As Integer
        Dim objItem As Object
        For i = 1 To StartFolder.Items.Count
            objItem = StartFolder.Items.Item(i)
            'Save this item 
            SaveItems(objItem, strPath)
            objItem = Nothing
        Next

        ' process all the subfolders of this folder
        Dim objFolder As Microsoft.Office.Interop.Outlook.MAPIFolder = Nothing
        For i = 1 To StartFolder.Folders.Count
            objFolder = StartFolder.Folders.Item(i)
            Dim strSubFolder As String
            strSubFolder = strPath & objFolder.Name
            Call ProcessFolder(objFolder, strSubFolder)
        Next
        objFolder = Nothing
        objItem = Nothing
    End Sub

    Private Sub SaveItems(ByVal objItem As Object, ByVal strFolderPath As String)
        Try
            Dim strSaveName As String = ""
            Dim entryId As String = ""
            Dim mailFrom As String = ""
            Dim mailTo As String = ""
            Dim mailCC As String = ""
            Dim mailBCC As String = ""
            Dim strSubject As String = ""
            Dim strBody As String = ""
            Dim pstDt As Date
            Dim recdDt As Date
            Dim i As Long
            Dim attchCount As Long

            Dim MItem As Microsoft.Office.Interop.Outlook.MailItem = Nothing
            MItem = CType(objItem, Microsoft.Office.Interop.Outlook.MailItem)
            If Not MItem Is Nothing Then
                entryId = MItem.EntryID
                strBody = MItem.Body
                strSubject = MItem.Subject
                mailTo = MItem.To()
                mailBCC = MItem.BCC()
                mailCC = MItem.CC()
                recdDt = MItem.ReceivedTime()
                pstDt = MItem.DeferredDeliveryTime()
                mailFrom = MItem.SenderName

                Dim EmailMessages As String = "EmailMessages"
                If Not Directory.Exists(strDest + "\" + EmailMessages) Then
                    Directory.CreateDirectory(strDest + "\" + EmailMessages)
                End If
                MsgPath = strDest + "\" + EmailMessages
                'Save message in text files
                Dim fs As FileStream = File.Create(MsgPath + "\" + cnt.ToString & ".txt")
                Dim st As New StreamWriter(fs)
                'st.WriteLine("From : " & mailFrom)
                'st.WriteLine("To : " & mailTo)
                'st.WriteLine("CC : " & mailCC)
                'st.WriteLine("BCC : " & mailBCC)
                'st.WriteLine("Subject : " & strSubject)
                'st.WriteLine()
                'st.WriteLine("Message Body :" & strBody)

                st.WriteLine("From : " & MItem.SenderName)
                st.WriteLine("To : " & MItem.To)
                st.WriteLine("CC : " & MItem.CC)
                st.WriteLine("BCC : " & MItem.BCC)
                st.WriteLine("Subject : " & MItem.Subject)
                st.WriteLine()
                st.WriteLine("Message Body :" & MItem.Body)

                st.WriteLine()
                st.AutoFlush = True
                st.Close()
                fs.Close()

                strSaveName = (cnt.ToString) & ".msg"

                'Save as message
                MItem.SaveAs(strFolderPath & "\" & (strSaveName))
                Dim tmpItem As Object
                'Mail Attachment collection object
                Dim tmpAttachs As Microsoft.Office.Interop.Outlook.Attachments
                'Single Attachment object
                Dim tmpAttach As Microsoft.Office.Interop.Outlook.Attachment

                tmpItem = mOulook.CreateItemFromTemplate(strFolderPath + "\" + strSaveName)
                tmpAttachs = tmpItem.Attachments
                attchCount = tmpAttachs.Count
                For i = 1 To attchCount
                    If Not Directory.Exists(strDest & "\" & cnt.ToString) Then
                        Directory.CreateDirectory(strDest & "\" & cnt.ToString)
                    End If
                    tmpAttach = tmpAttachs.Item(i)
                    tmpAttach.SaveAsFile(strDest & "\" & cnt.ToString + "\" + tmpAttach.FileName)
                Next
                cnt += 1
            End If
        
        Catch ex As System.Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private disposedValue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedValue Then
            If disposing Then
                ' TODO: free unmanaged resources when explicitly called
                Try
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(objApp)
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(mOulook)
                    objApp = Nothing
                    mOulook = Nothing
                    System.GC.Collect()
                Catch ex As SystemException

                End Try
            End If

            ' TODO: free shared unmanaged resources
        End If
        Me.disposedValue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

    
End Class
